#include <iostream>
using namespace std;
int main()
{
int a,b;
int multip;
cout << "Enter a: ";
cin >> a;
cout << "Enter b: ";
cin >> b;

if((a != 0) && (b != 0))
{
    a = (a > 0 ? a : -1*a);
    b = (b > 0 ? b : -1*b);

    multip = a*b;
    int t;

    if(a < b)
    {
        t = a;
        a = b;
        b = t;
    }

    t = a % b;
    while (t != 0)
    {
        a = b;
        b = t;
        t = a % b;
    }
    cout << "NOD: " << b << endl;
    cout << "NOK: " << multip / b << endl;
}
else
{
    if ((a == 0) && (b == 0))
    {
        cout << "Can't find NOD" << endl;
    }
    else
    {
        cout << "NOD: " << (a != 0 ? a : b) << endl;
    }
    cout << "Can't find NOK" << endl;
}
return 0;
}
