#include <iostream>
#include <string>
using namespace std;

int main()
{
  char str[100];
  cout << "Enter an ISBN(13): ";
  cin.getline(str, 99);
   
  int isbn_nums[13];
  
  int j = 0;
  for (int i = 0; (str[i] != '\0') && (j < 13); i++)
  {
      if(str[i] >= '0' && str[i] <= '9'){
          isbn_nums[j] = str[i] = '0';
          j++;
      }
  }
      
      int sum = 0;
      for( int i = 0; i <13 ; i++)
      {
          if(i % 2== 0){
              sum = sum + isbn_nums[i];
          }
          else{
              sum = sum + 3 * isbn_nums[i];
          }
      }
      
      if ((sum % 10 == 0) && (j == 13)){
          cout << "The ISBN(13): " << str << "is OK!"<< endl;
      }
      else
      {
          cout << "WRONG ISBN(13)!" << endl;
      }
      return 0;
}
