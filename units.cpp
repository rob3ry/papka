/* 
Output: Enter a number in meters;
input: 600 
Output:
600 m = 0.6 km
600 m = 6000 dm
600 m = 60000 cm*/


#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int a = 0;
   cout << "Enter a number in meters: ";
   cin >> a;
   cout << a << " m = " << a / 1000. << " km" <<endl;
   cout << a << " m = " << a * 10 << " dm" <<endl;
   cout << a << " m = " << a * 100 << " cm" <<endl;
   
  return 0;
}
