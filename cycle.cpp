#include <iostream>
#include <limits>
using namespace std;
int main()
{
    int x;
    int mini = numeric_limits<int>::max();
    int maxi = numeric_limits<int>::min();

    while(cin >> x)
    {
        if( mini > x) mini = x;
        if( maxi < x) maxi = x;
    }
    cout << "min value = " << mini << endl;
    cout << "max value = " << maxi;

}
