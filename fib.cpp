#include <iostream>
using namespace std;

int f_fib(int n)
{
     int fibres = 0;

     if(n < 3)
     {
         fibres = 1;
     }
     else
     {
         int i = 2;
         int f1 = 1;
         int f2 = 1;

         while(i < n)
         {
             fibres = f1 + f2;
             f1 = f2;
             f2 = fibres;
             i++;
         }
     }

     cout << "Fib(" << n << ") = " << fibres << endl;
     return fibres;
}
int f_nod(int a, int b)
{
    int res_nod = -1;
    int multip = a*b;
if((a != 0) && (b != 0))
{
    a = (a > 0 ? a : -1*a);
    b = (b > 0 ? b : -1*b);

    multip = a*b;
    int t;

    if(a < b)
    {
        t = a;
        a = b;
        b = t;
    }

    t = a % b;
    while (t != 0)
    {
        a = b;
        b = t;
        t = a % b;
    }
    cout << "NOD: " << b << endl;
    res_nod = b;
    cout << "NOK: " << multip / b << endl;
}
else
{
    if ((a == 0) && (b == 0))
    {
        cout << "Can't find NOD" << endl;
    }
    else
    {
        cout << "NOD: " << (a != 0 ? a : b) << endl;
    }
    cout << "Can't find NOK" << endl;
}
}
int main()
{
    int n;
    cout << "Enter Fib.sequence num: ";
    cin >> n;
    int fibn = f_fib(n);


    int m;
    cout << "Enter Fib.sequence num: ";
    cin >> m;
    int fibm = f_fib(m);

    cout << "F(" << n << ") = " << fibn << endl;
    cout << "F(" << m << ") = " << fibm << endl;

    int nodnm = f_nod(n, m);
    int nodfnfm = f_nod(fibn, fibm);
    int fibnodnm = f_fib(nodnm);

    if(nodfnfm == fibnodnm)
    {
        cout << "Bravo!";
    }
    return 0;



}
