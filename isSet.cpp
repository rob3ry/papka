/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>

using namespace std;

bool isSet(int* inSet, int nSize);
void makeSet(int* inSet, int inSize, int*& outSet, int& outSize);
int main()
{
    int n;
    cout << "Enter number of elements: ";
    cin >> n;
    
    int* u_set = new int[n];
    cout << "Enter " << n << " values: ";
    for(int i = 0 ; i < n; i++)
    {
        cin >> u_set[i];
    }
    for (int i = 0;i < n; i++)
    {
        cout << u_set[i] << " ";
    }
    
    bool isSetVal = isSet(u_set, n);
    
    cout << endl;
    cout << "It is " << (isSetVal ? "" : "not") << " a set";
    
    int* out = NULL;
    int oS = 0;
    
    makeSet(u_set,n ,out ,oS);
    for(int i = 0; i < oS; i++)
    {
        cout << out[i] << "; ";
    }
    return 0;
}

bool isSet(int* inSet, int nSize)
{
    bool res = true;
    for(int i = 0; i < nSize ; i++)
    {
        for( int j = i + 1; j < nSize ; j++)
        {
            if(inSet[i] == inSet[j])
            {
                return false;
            }
        }
    }
    return true;
}
void makeSet(int* inSet, int inSize, int*& outSet, int& outSize)
{
    outSize = inSize;
    
    for(int i = 0; i < inSize ; i++)
    {
        for(int j = i+1; j < inSize; j++)
        {
            if(inSet[i] == inSet[j])
            {
                outSize--;
                break;
            }
        }
    }
    outSet = new int[outSize];
    
    int count = 0;
    int eql = false;
    for(int i = 0; i < inSize; i++)
    {
        eql = false;
        for(int j= 0; j < count; j++)
        {
            eql = eql || (inSet[i] == outSet[j]);
        }
        if (!eql)
        {
            outSet[count] = inSet[i];
            count++;
        }
    }
}




