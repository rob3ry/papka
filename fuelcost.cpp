/* 
Output: Enter a tank size;
input: 50
Output: Enter fuel efficiency (km per liter):
Input: 10
Output: Enter price per liter:
Input: 2.45
Output: We can travel 500 km.   tank * eff
For every 100km it will cost 24.5lv.  price * eff
*/


#include <iostream>
#include <cmath>
using namespace std;

int main()
{
  int tsize = 0;
  cout << "Enter a tank size: " << endl;
  cin >> tsize;
  
  int eff = 0;
  cout << "Enter fuel efficiency (km per liter): " << endl;
  cin >> eff;
  
  double price = 0;
  cout << "Enter price per liter: " << endl;
  cin >> price;
  
  cout << "We can travel " << tsize * 100/ eff << " km " << endl;
  cout << "For every 100 km it will cost " << price * eff << " lv ";
  return 0;
}
